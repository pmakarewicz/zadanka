### practicepython.org
### 6. String lists

i=0
counter = 0
word = str(input("Enter your word: "))

for element in word:
    i += 1
    if element == word[len(word)-i]:
        counter += 1

if counter == len(word):
    print("Your word is a palindrome!")
else:
    print ("Your word is not a palindrome!")