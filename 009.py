### practicepython.org
### 8. Guessing Game One

import random
guess = random.randint(1,9)

while True:
    liczba = int(input("Podaj swoja liczbe: "))
    if liczba < guess:
        print("Twoja liczba jest za mala!")
    if liczba > guess:
        print ("Twoja liczba jest za duza!")
    if liczba == guess:
        print ("GRATULUJE! Zgadles swoja liczbe!")
        odp = str(input("Czy chcesz zagrac od nowa? "))
        if odp == "tak":
            guess = random.randint(1, 9)
        else:
            break