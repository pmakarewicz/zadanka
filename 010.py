### practicepython.org
### 11. Check Primality

number = int(input("Podaj swoja liczbe: "))
counter = 2
divcounter = 0
list = []
while counter < number:
    list.append(counter)
    counter += 1

for element in list:
    if number % element == 0:
        divcounter += 1

if divcounter == 0:
    print("Twoja liczba jest liczba pierwsza!")
else:
    print("Twoja liczba nie jest liczba pierwsza!")